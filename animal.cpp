///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Shaun Corpuz <corpuzsa@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   02/27/2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <stdlib.h>

#include "animal.hpp"

using namespace std;

namespace animalfarm {

// constructor 
// no endl, we want it in one line
Animal::Animal(){
   cout << ".";
}

// destructor
// no endl, we want it in one line
Animal::~Animal(){
   cout << "X";
}

void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}

// random color
const Color Animal::getRandomColor(){
   int color = rand() % 6;

   switch(color){
      case 0:        return BLACK;              break;
      case 1:        return WHITE;              break;
      case 2:        return RED;                break;
      case 3:        return SILVER;             break;
      case 4:        return YELLOW;             break;
      case 5:        return BROWN;              break;
   }
   return BLACK;
}

// random bool
const bool Animal::getRandomBool(){
   int tof = rand() % 2;
   
   switch(tof){
      case 0:        return false;              break;
      case 1:        return true;               break;
   }
   
   return true;
}

// random gender
const enum Gender Animal::getRandomGender(){
   int gender = rand() % 2;

   switch(gender){
      case 0:        return MALE;               break;
      case 1:        return FEMALE;             break;
   }

   return MALE;
}

// random weight
const float Animal::getRandomWeight(const float from, const float to){

   int range = (int) (to - from);
   float randomWeight = from + (rand() % range);

   return randomWeight;
}

// random name
const string Animal::getRandomName(){
   int length = (rand() % 6) + 4;
   char randomName[length];

   // uppercase
   randomName[0] = (char) (65 + rand() % 26);

   // lowercase
   for (int i=1;i < length;i++){
      randomName[i] = (char) (97 + rand() % 26);
   }

   return randomName;
}

// Gender enum to string
string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:     return string("Male");     break;
      case FEMALE:   return string("Female");   break;
      case UNKNOWN:  return string("Unknown");  break;
   }

   return string("Really, really Unknown");
};
	
// Color enum to string
string Animal::colorName (enum Color color) {
   switch(color){
      case BLACK:    return string("Black");    break;
      case WHITE:    return string("White");    break;
      case RED:      return string("Red");      break;
      case SILVER:   return string("Silver");   break;
      case YELLOW:   return string("Yellow");   break;
      case BROWN:    return string("Brown");    break;
   }
   return string("Unknown");
};
	
} // namespace animalfarm
